package com.entersekt.dockerls.unittests;

import com.entersekt.dockerls.application.services.DirectoryListingServiceImp;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class DirectoryListingServiceTests {

    private DirectoryListingServiceImp directoryListingServiceImp = new DirectoryListingServiceImp();


    @Test
    void isValidDirectory_whenPathExceedsLength_badRequestExceptionThrown() {

        String path = StringUtils.repeat('a', 1025);

        Exception exception = assertThrows(RuntimeException.class, () -> directoryListingServiceImp.isValidDirectory(path));

        String expectedMessage = "path can not be greater than 1024 characters";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void isValidDirectory_whenPathIsEmpty_badRequestExceptionThrown() {

        String path = "";

        Exception exception = assertThrows(RuntimeException.class, () -> directoryListingServiceImp.isValidDirectory(path));

        String expectedMessage = "path can not be empty";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void isValidDirectory_validpath_returnsTrue() {

        String path = "src";

        Boolean isValidPath = directoryListingServiceImp.isValidDirectory(path);

        assertTrue(isValidPath);
    }
}