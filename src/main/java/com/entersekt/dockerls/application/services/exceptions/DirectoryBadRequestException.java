package com.entersekt.dockerls.application.services.exceptions;

public class DirectoryBadRequestException extends RuntimeException{

    public DirectoryBadRequestException(String message) {
        super(message);
    }
}
