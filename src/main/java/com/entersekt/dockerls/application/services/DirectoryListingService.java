package com.entersekt.dockerls.application.services;

import com.entersekt.dockerls.application.services.exceptions.DirectoryBadRequestException;
import com.entersekt.dockerls.application.services.exceptions.DirectoryNotFoundException;
import com.entersekt.dockerls.domain.DirectoryListing;

public interface DirectoryListingService {

    Boolean isValidDirectory(String path) throws DirectoryBadRequestException;

    DirectoryListing getDirectoryListing(String path) throws DirectoryNotFoundException;
}
