package com.entersekt.dockerls.application.services;

import com.entersekt.dockerls.application.services.exceptions.DirectoryBadRequestException;
import com.entersekt.dockerls.application.services.exceptions.DirectoryNotFoundException;
import com.entersekt.dockerls.domain.DirectoryListing;
import org.springframework.stereotype.Service;

@Service
public class DirectoryListingServiceImp implements DirectoryListingService{



    @Override
    public Boolean isValidDirectory(String path) throws DirectoryBadRequestException {

        if (path.isEmpty()) {
            throw new DirectoryBadRequestException("path can not be empty");
        }

        if (path.length() > 1024) {
            throw new DirectoryBadRequestException("path can not be greater than 1024 characters");
        }

        return true;
    }

    @Override
    public DirectoryListing getDirectoryListing(String path) throws DirectoryNotFoundException {
        return null;
    }
}
