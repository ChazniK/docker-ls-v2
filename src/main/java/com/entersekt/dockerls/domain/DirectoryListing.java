package com.entersekt.dockerls.domain;

import java.util.List;

public class DirectoryListing {

    private String fullPath;
    private int size;
    private List<DirectoryEntry> directoryEntryList;

    public DirectoryListing(String fullPath, int size, List<DirectoryEntry> directoryEntryList) {
        this.fullPath = fullPath;
        this.size = size;
        this.directoryEntryList = directoryEntryList;
    }

    public String getFullPath() {
        return fullPath;
    }

    public int getSize() {
        return size;
    }

    public List<DirectoryEntry> getDirectoryEntryList() {
        return directoryEntryList;
    }
}
