package com.entersekt.dockerls.domain;


public class DirectoryEntry {

    private String name;
    private int size;

    public DirectoryEntry(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }
}
